# all_in_one.sh

all_in_one.sh is designed combine all the important metrics needed for reviewing server performance, into one nicely formatted output, convenient for displaying to clients or assisting with troubleshooting. The idea behind this is that you can either run the script with flags to quickly pull the information that's relevant to your use-case, or you can pull a list of most of the important server stats to see when troubleshooting issues.

The script has various options that can be invoked to specifically pull the details you need:

standalone - all data
-g - GENERAL data
-d - DISK data
-m - MEMORY data
-c - CPU data
-t - TRAFFIC data

