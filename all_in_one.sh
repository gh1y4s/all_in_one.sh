#!bin/bash
##add flag capability

##FORMATTING##
categorybreaker=$(seq $COLUMNS | awk '$1="="' | tr -d '\n')
sectionbreaker=$(seq $COLUMNS | awk '$1="-"' | tr -d '\n')
echo -e "$categorybreaker\n$categorybreaker\nALL IN ONE\n$categorybreaker\n$categorybreaker"


##GENERAL##
#atopsar avg & peak load times
general=$(w | grep -i up)
os=$(grep -i "pretty\|name\|version\|id" /etc/os-release)
WORDTOREMOVE="PRETTY_NAME="
last=$(last -10 | head -n10)

echo -e "GENERAL\n$general\n$sectionbreaker\nOPERATING SYSTEM"
echo -e "$os" | sed s/"$WORDTOREMOVE"// | head -n1
echo -e "$sectionbreaker\nLAST\n$last\n$sectionbreaker"

##ATOPSAR LOAD AVG SCRIPT##
#!/bin/bash

ls -l /var/log/atop | awk '{print $9}' | grep -v 'daily' > /tmp/file.txt

sed -i '1d' /tmp/file.txt
sed -i 's#^#/var/log/atop/#' /tmp/file.txt

while read i; do
atopsar -p -r $i -b 00:00 -e 23:59 | awk 'BEGIN {DATE_STAMP=""; } /analysis date: /{DATE_STAMP=$4;} /^[0-9]/ {print DATE_STAMP, $0;}' >> /tmp/atopsar.report
done < /tmp/file.txt

grep -v "analysis date:" /tmp/atopsar.report > /tmp/atopsar_numbers.report

echo -e "Peak Server Load Times\nDate       Time     Load"
sort -k7 -rn /tmp/atopsar_numbers.report | head -n5 | awk '{print $1,$2,$7}'

rm -rf /tmp/atopsar*

##End of ATOPSAR LOAD AVG SCRIPT##


##DISKS##
##fix disk health output, atopsar disk io avg & peak times,
inodes=$(df -i)
disk_space=$(df -h)
disk_health=$(wget -q https://www.hdsentinel.com/hdslin/hdsentinel-018c-x64.gz && gunzip hdsentinel-018c-x64.gz && chmod +x ./hdsentinel-* && ./hdsentinel-018c-x64 -v -r disk_health_report.txt | grep -i "DISK DEVICE\|DEVICE TYPE\|REVISION\|TOTAL SIZE\|HEALTH\|PERFORMANCE\|STATUS\|POWER\|LIFETIME" | grep -vi 'report' && rm -f disk_health_report.txt && rm -rf hdsentinel-*)

echo -e "$categorybreaker\nDISKS\n$categorybreaker\nDISK USAGE\n$disk_space\n$sectionbreaker\nINODE USAGE\n$inodes\n$sectionbreaker\nDISK HEALTH\n$disk_health\n$sectionbreaker"

##DISKS - ATOPSAR SCRIPT##

##ATOPSAR DISK SCRIPT##
#!/bin/bash

ls -l /var/log/atop | awk '{print $9}' | grep -v 'daily' > /tmp/file.txt

sed -i '1d' /tmp/file.txt
sed -i 's#^#/var/log/atop/#' /tmp/file.txt

while read i; do
atopsar -S -l -r $i -b 00:00 -e 23:59 | awk 'BEGIN {DATE_STAMP=""; } /analysis date: /{DATE_STAMP=$4;} /^[0-9]/ {print DATE_STAMP, $0;}' >> /tmp/atopsar.report
done < /tmp/file.txt

grep -v "analysis date:" /tmp/atopsar.report > /tmp/atopsar_numbers.report

echo -e "Peak Write IOPS\nDate       Time     Write/s"
sort -k7 -rn /tmp/atopsar_numbers.report | head -n5 | awk '{print $1,$2,$7}'
echo -e "Peak Read IOPS\nDate       Time     Read/s"
sort -k5 -rn /tmp/atopsar_numbers.report | head -n5 | awk '{print $1,$2,$5}'

rm -rf /tmp/atopsar*

##End of ATOPSAR DISK SCRIPT##

##MEMORY##
##atopsar avg & peak memory usage?, possibly way to detect memory errors from cli?
current_memory=$(free -h)
swappiness=$(cat /proc/sys/vm/swappiness)
echo -e "$categorybreaker\nMEMORY\n$categorybreaker\nCURRENT MEMORY USAGE\n$current_memory\n$sectionbreaker\nSWAPPINESS\n$swappiness\n$sectionbreaker"

##MEMORY - ATOPSAR SCRIPT##

ls -l /var/log/atop | awk '{print $9}' | grep -v 'daily' > /tmp/file.txt

sed -i '1d' /tmp/file.txt
sed -i 's#^#/var/log/atop/#' /tmp/file.txt

while read i; do
atopsar -m -r $i | awk 'BEGIN {DATE_STAMP=""; } /analysis date: /{DATE_STAMP=$4;} /^[0-9]/ {print DATE_STAMP, $0;}' >> /tmp/atopsar.report
done < /tmp/file.txt

header=$(awk 'FNR == 6 {print}' /tmp/atopsar.report | sed s/"dirty"// | sed s/"_mem"// | sed s/"slabmem"// | cut -f1 -d" " --complement | sed s/"buffers"//)

grep -v "analysis date:" /tmp/atopsar.report > /tmp/atopsar_numbers.report

echo -e "Peak Memory Usage Times\ndate       time     total used  cache swap  swap-free"

sort -k3 -rn /tmp/atopsar_numbers.report | head -n5 | awk '{for(i=1;i<=NF;i++)if(i!=5 && i!=7 && i!=8)printf "%s ",$i;print""}'

rm -rf /tmp/atopsar*

echo -e "$categorybreaker"
##End of ATOPSAR MEMORY SCRIPT##

##TRAFFIC##
##abuseIPDB API, review Nick Abbots script, max_children, maxrequestworkers.


##NETWORK##
##open ports, services listening on which ports, atopsar network stats

echo -e "NETWORK\n$categorybreaker\nListening Services"
lsof -i -P -n | awk '{print $1,$3,$5,$8,$9}'
echo -e "$categorybreaker"

##NETWORK - ATOPSAR DISK SCRIPT##
#!/bin/bash

ls -l /var/log/atop | awk '{print $9}' | grep -v 'daily' > /tmp/file.txt

sed -i '1d' /tmp/file.txt
sed -i 's#^#/var/log/atop/#' /tmp/file.txt

while read i; do
atopsar -i -r $i -b 00:00 -e 23:59 | awk 'BEGIN {DATE_STAMP=""; } /analysis date: /{DATE_STAMP=$4;} /^[0-9]/ {print DATE_STAMP, $0;}' >> /tmp/atopsar.report
done < /tmp/file.txt

grep -v "analysis date:" /tmp/atopsar.report > /tmp/atopsar_numbers.report

echo -e "Peak Network Input \nDate       Time     Int   ipack/s"
sort -k5 -rn /tmp/atopsar_numbers.report | head -n5 | awk '{print $1,$2,$3,$5}'
echo -e "Peak Network Output\nDate       Time     Int   opack/s"
sort -k6 -rn /tmp/atopsar_numbers.report | head -n5 | awk '{print $1,$2,$3,$6}'

rm -rf /tmp/atopsar*

##End of ATOPSAR NETWORK SCRIPT##
echo -e "$categorybreaker"

